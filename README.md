# CONGESTION CONTROL USING AODV PROTOCOL SCHEME FOR WIRELESS AD-HOC NETWORK
## AGENT-BASED CONGESTION CONTROL


## Anggota
1. Faizal Khilmi Muzakki **05111640000120**
2. Muhammad Muzayyin Amrullah **05111640000095**
3. Muhammad Iqbal Izzul Haq **05111640000190**
4. Itsna Dzakiaatul Huriroh **05111640007001**


## Overview
- [1. CONGESTION CONTROL USING AODV PROTOCOL SCHEME FOR WIRELESS AD-HOC NETWORK](#congestion-control-using-aodv-protocol-scheme-for-wireless-ad-hoc-network)
    - [Overview](#overview)
    - [1. Konsep](#1-konsep)
    - [1.1 Deskripsi Paper](#11-deskripsi-paper)
    - [1.2 Latar Belakang](#12-latar-belakang)
    - [1.3 Dasar Teori](#13-dasar-teori)
      - [1.3.1 AODV Routing Protocol](#131-aodv-routing-protocol)
    - [1.4 Modified AODV](#14-routing-protokol-yang-diusulkan)
      - [1.4.1 Agent-Based Congestion Control](#141-agent-based-congestion-control)
- [2. Implementasi](#2-implementasi)
  - [2.1 Deskripsi Sistem](#21-deskripsi-sistem)
  - [2.2 Modifikasi](#22-modifikasi)
- [3. Testing](#3-testing)
- [4. Referensi](#4-referensi)

## 1. Konsep
### 1.1 Deskripsi Paper
Implementasi MAODV didasarkan pada paper berikut :
* Judul : **CONGESTION CONTROL USING AODV PROTOCOL SCHEME FOR WIRELESS AD-HOC NETWORK**
* Penulis : M. Rajesh, J. M. Gnanasekar
* Tahun : Received: March 15, 2015, Accepted: March 28, 2016, Published: November 7, 2016, Available Online: August 2016.
* Sumber : https://www.researchgate.net/publication/306041743


### 1.2 Latar Belakang
* Pada saat ini, **AODV** merupakan _routing_ protokol yang sering digunakan pada _Ad-Hoc Network_. Tetapi masalah terbesarnya adalah _delay_. Pada fase pencarian rute dan _maintenance_ rute, **sejumlah besar data** dikirim melalui rute dengan **jumlah node yang relatif sedikit** sehingga menyebabkan **kongesi dan _bottleneck_** pada jaringan. Dengan meningkatnya jumlah node _brownout_, konektivitas jaringan akan melemah dan keseluruhan _survival time_ dari jaringan tersebut akan menurun juga.
* Oleh karena itu, untuk **menyeimbangkan beban jaringan** dan **menjaga jaringan** secara kontinu, efisien, dan stabil, adalah hal yang penting untuk memperhatikan beban _routing node_ dan kongesi di dalam jaringan tsb. Pada jaringan nirkabel ad-hoc, **_wireless agent_ (WA)** memiliki mobilitas dan otonomi yanga dapat digunakan untuk **memecahkan kongesi** pada jaringan Ad-Hoc.

### 1.3 Dasar Teori
#### 1.3.1 AODV Routing Protocol
* **AODV** *(Ad Hoc On-Demand Distance Vector Routing Protocol)* adalah salah satu *reactive routing protokol* yang memiliki 3 jenis paket untuk **route discovery** dan **route maintenance**, yakni:
  * Route Request (RREQ)
    Memiliki format paket sebagai berikut:
  
    ![Paket RREQ](/img/rreq.jpg)

    Keterangan:
      * **Type** : Tipe paket (RREQ = 1)
      * **|J|R|G|D|U|** : Flag yang menunjukkan keadaan paket
      * **Reserved** : Default 0, diabaikan
      * **Hop Count** : Jumlah hops dari node asal ke node yang sedang meng-handle RREQ saat ini
      * **RREQ ID** : Nomor unik untuk mengidentifikasi RREQ tertentu 
      * **Destination IP Address** : Alamat IP tujuan
      * **Destination Sequence Number** : Urutan node tujuan
      * **Originator IP Address** : Alamat IP asal yang menginisialisasi RREQ
      * **Originator Sequence Number** : Urutan node asal


  * Route Reply (RREP)
    Memiliki format paket sebagai berikut:

    ![Paket RREP](/img/rrep.jpg)


  * Route Error (RERR) 
    Memiliki format paket sebagai berikut:

    ![Paket RERR](/img/rerr.jpg)

* Cara kerja AODV :
  1. Ketika node asal ingin berkomunikasi dengan node tujuan yang belum memiliki rute, maka **node asal** akan mem-broadcast RREQ ke jaringan untuk melakukan **route discovery** (pencarian rute).
  2. Ketika **node intermediate** menerima paket RREQ yang belum pernah diterima sebelumnya (cek RREQ ID), maka ia akan membuat **reverse route** ke node asal. 
  3. **Node intermediate** akan mengecek alamat tujuan pada paket RREQ. Jika ia belum punya informasi rute ke node tujuan yang dimaksudkan, maka ia akan menambah hop count pada paket dan meneruskan broadcast RREQ ke jaringan. Namun jika sebaliknya, ia akan mengirimkan **RREP** ke node asal.
  4. Ketika **node asal** menerima RREP, ia akan menyimpan rute tersebut dan memulai transmisi data.
  

### 1.4 Routing Protokol yang Diusulkan
#### 1.4.1 Agent-Based Congestion Control
* Tujuan dari ABCC adalah :
  * Memilih rute dengan Metriks Kongesi paling minimum, sehingga rute yang digunakan lebih reliable dan delay yang harusnya lebih rendah dari rute lain.

* Untuk mencapai tujuan tersebut, maka perlu dihitung Total Congestion Metrics (TCM) untuk setiap rute dengan rumus berikut: ![TCM Formula](/img/tcm.PNG)
  * Dengan Cocc (_Channel Contention Estimation_) yang dapat didapatkan dengan: ![TCM Formula](/img/cocc.PNG)
  * Dengan PR(Q1) (_Queue Length Estimation_) yang dapat didapatkan dengan: ![TCM Formula](/img/qle.PNG)
  
* Cara kerja ABCC:
  * 1. _Source_ S mengecek jumlah tetangga satu hop yang tersedia dan mengkloning _Wireless Agent_(MA) pada tetangga-tetangga tsb.
  * 2. masing-masing MA mencari jalur terpendek menuju _destination_ D.
  * 3. MA1 bergerak menuju D dengan cara _hop-by-hop_ pada jalur P1, MA2 pada P2, dst..
  * 4. MA1 menghitung TCM1 dari jalur P1, MA2 menghitung TCM2 dari jalur P2, dst.
  * 5. _destination_ D mengirim TCM1, TCM2, ... dari jalur P1, P2, ... ke _source_ S.
  * 6. S memilih jalur menggunakan min(TCM1, TCM2, ...) dan mengirim data melewati jalur dengan kongesi paling minimum.

## 2. Implementasi
### 2.1 Deskripsi Sistem
Implementasi routing protocol MAODV - ABCC dilakukan pada sistem dengan
* Sistem Operasi : Ubuntu 18.04
* Aplikasi yang digunakan :
  * NS-2.35
  <!-- * Netbeans for C/C++ Development ([Instalasi](https://websiteforstudents.com/how-to-install-netbeans-on-ubuntu-16-04-17-10-18-04/)) -->
  
## 3. Testing

## 4. Referensi
* https://www.researchgate.net/publication/306041743
* intip.in/WORKSHOPNS2